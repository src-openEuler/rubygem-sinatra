%global gem_name sinatra
%bcond_with bootstrap
Summary:             Ruby-based web application framework
Name:                rubygem-%{gem_name}
Version:             2.0.8.1
Release:             4
License:             MIT
URL:                 http://www.sinatrarb.com/
Source0:             https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/sinatra/sinatra.git && cd sinatra
# git archive -v -o sinatra-2.0.8.1-test.tar.gz v2.0.8.1 test/
Source1:             %{gem_name}-%{version}-test.tar.gz
# Fix test failure due to Rack 2.2.2 incompatibility.
# https://github.com/sinatra/sinatra/pull/1605
Patch0:              rubygem-sinatra-2.0.8.1-Fix-failing-tests.patch
Patch1:              Internal-Sinatra-errors-now-extend-Sinatra-Error-test.patch
Patch2:              Internal-Sinatra-errors-now-extend-Sinatra-Error.patch
Patch3:              Fix-broken-spec.patch

# Security fix
Patch3000:           backport-CVE-2022-45442.patch
Patch3001:           backport-CVE-2022-45442-test.patch

BuildRequires:       rubygems-devel ruby(release) ruby >= 2.2.0
%if ! 0%{?bootstrap}
BuildRequires:       rubygem(rack) >= 2.0 rubygem(rack-protection) = %{version} rubygem(tilt)
BuildRequires:       rubygem(mustermann) rubygem(rack-test) rubygem(minitest) > 5
%endif
Epoch:               1
BuildArch:           noarch
%description
Sinatra is a DSL intended for quickly creating web-applications in Ruby
with minimal effort.

%package help
Summary:             Documentation for %{name}
Requires:            %{name} = %{epoch}:%{version}-%{release}
Provides:            %{name}-doc = %{version}-%{release}
Obsoletes:           %{name}-doc < %{version}-%{release}
%description help
This package contains documentation for %{name}.

%prep
%setup -q -n %{gem_name}-%{version} -b 1

pushd %{_builddir}
%patch0 -p1
%patch1 -p1
%patch3 -p1
%patch3001 -p1
popd

%patch2 -p1
%patch3000 -p1

%build
gem build ../%{gem_name}-%{version}.gemspec
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -rv .%{gem_dir}/* %{buildroot}%{gem_dir}
sed -i -e 's|^#!/usr/bin/env ruby|#!/usr/bin/ruby|' \
  %{buildroot}%{gem_instdir}/examples/*.rb

%if %{without bootstrap}
%check
pushd .%{gem_instdir}
cp -a %{_builddir}/test test
sed -i '/active_support/ s/$/ unless Hash.method_defined?(:slice)/' test/helper.rb
mv test/integration_test.rb{,.disabled}
ruby -e 'Dir.glob "./test/*_test.rb", &method(:require)'
popd
%endif

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.*
%license %{gem_instdir}/LICENSE
%{gem_libdir}
%exclude %{gem_instdir}/sinatra.gemspec
%exclude %{gem_cache}
%{gem_spec}

%files help
%doc %{gem_docdir}
%doc %{gem_instdir}/AUTHORS.md
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/CONTRIBUTING.md
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/MAINTENANCE.md
%doc %{gem_instdir}/README*.md
%{gem_instdir}/Rakefile
%doc %{gem_instdir}/SECURITY.md
%{gem_instdir}/VERSION
%{gem_instdir}/examples

%changelog
* Mon Nov 18 2024 yaoxin <yao_xin001@hoperun.com> - 1:2.0.8.1-4
- Fix CVE-2022-45442

* Sat May 11 2024 Zhengxin Guo <guozhengxin@kylinos.cn> - 1:2.0.8.1-3
- fix the correctly ignores syntactically invalid range requests test

* Tue Jan 17 2023 wulei <wulei80@h-partners.com> - 1:2.0.8.1-2
- fix the error_highlight test

* Thu Feb 24 2022 liyanan <liyanan32@huawei.com> - 2.0.8.1-1
- update to 2.0.8.1

* Wed Feb 10 2021 Ge Wang <wangge20@huawei.com> - 2.0.3-2
- solve test error due to rubygem-rack update to 2.2.0+

* Sat Aug 22 2020 liyanan <liyanan32@huawei.com> - 2.0.3-1
- package init
